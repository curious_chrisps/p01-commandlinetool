//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
using namespace std;

// FUNCTION DECLARATION
void doTheFactorial (int input); // Calculates the factorial of a given positive value.
void validate (int* toValidate); // Validates intput to be positive.

int main()
{   // insert code here...
    int toCalculate;
    
    // Prompt to enter value (cout line) and getting user's input (cin line).
    cout << "Enter an integer number to calculate its factorial: " << endl << ">>> ";
    cin >> toCalculate;
    
    // Call to validate function, then call to the calculation function.
    validate (&toCalculate);
    doTheFactorial (toCalculate);
    
    return 0;
} //end main function



// FUNCTION DEFINITION
void doTheFactorial(int input)
{
    unsigned long long result = 1;
    
    for (int i = input; i >= 1; i--)
    {
        result *= i;
    }
    cout << "The factorial of " << input << " is: " << result << endl;
}



void validate (int* toValidate)
{
    // If the user enters a value less than less than or equal to 0, the program executes this loop until the input is positive.
    while(*toValidate <= 0)
    {
        cout << "Enter positive value." << endl << ">>> ";
        cin >> *toValidate;
    }
}
